#include "actor.hpp"

Actor::Actor(const Animation* animation) :
	m_velocity(0.0f, 0.0f),
	m_animationState(animation)
{
}

Actor::~Actor()
{
}

void Actor::update()
{
	move(m_velocity);
	m_animationState.update();
}