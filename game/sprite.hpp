#pragma once

#include <unordered_map>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics.hpp>
#include <json/json.h>

class Sprite;
typedef std::vector<Sprite> SpriteGroup;

class Sprite
{
public:
	struct Sheet : sf::NonCopyable
	{
		Sheet();
		Sheet(Sheet&& other);

		sf::Texture texture;
		std::vector<Sprite> sprites;
	};

	Sprite(const sf::Texture& texture, const sf::FloatRect& rect);
	Sprite(const Sprite& other);
	virtual ~Sprite();

	void draw(sf::RenderTarget& target, const sf::Transform& transform, sf::RenderStates states = sf::RenderStates::Default) const;

	static std::unordered_map<std::string, Sheet> loadSpriteSheets(const Json::Value& spriteSheetsDesc);

protected:
	const sf::Texture& m_texture;
	const sf::FloatRect m_rect;
};