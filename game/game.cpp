#include "game.hpp"
#include <sstream>
#include <fstream>
#include <exception>
#include <SFML/System/Clock.hpp>
#include "animation.hpp"

Game::Game(sf::VideoMode videoMode, const std::string& title) :
	m_window(videoMode, title),
	m_drawFps(true),
	m_frameId(0),
	m_fpsClock(),
	m_tpf(0),
	m_fpsFont()
{
	m_window.setKeyRepeatEnabled(false);
	loadConfig();
}

Game::~Game()
{

}

void Game::run()
{
	static const size_t k_maxFPS = 50;
	static const sf::Time k_minTPF = sf::seconds(1.0f / k_maxFPS);

	sf::Clock clock;
	for (m_frameId = 0; m_window.isOpen(); ++m_frameId)
	{
		sf::Event ev;
		while (m_window.pollEvent(ev))
		{
			handleEvent(ev);
		}

		update();
		m_window.clear();
		draw();

		sf::Time deltaT = clock.getElapsedTime();
		if (deltaT < k_minTPF)
		{
			sf::sleep(k_minTPF - deltaT);
		}

		clock.restart();
		m_window.display();
	}
}

void Game::loadConfig()
{
	Json::Value root;
	if (!Json::Reader().parse(std::ifstream("config.json"), root))
	{
		throw std::runtime_error("Error parsing config file.");
	}

	loadAssets(root["assets"]);
}

void Game::loadAssets(const Json::Value& assetsDesc)
{
	if (!m_fpsFont.loadFromFile(assetsDesc["font"].asString()))
	{
		throw std::runtime_error("Error loading font.");
	}

	m_spriteSheets = Sprite::loadSpriteSheets(assetsDesc["sprite-sheets"]);
	m_animations = Animation::loadAnimations(assetsDesc["animation-groups"], m_spriteSheets);
}

void Game::handleEvent(const sf::Event& ev)
{
	switch (ev.type)
	{
		case sf::Event::EventType::Closed:
			m_window.close();
			break;
	}
}

void Game::update()
{
	sf::Vector2f pos(0.0f, 100.0f);
	for (auto& actor : m_actors)
	{
		actor.second.update();
	}
}

void Game::draw()
{
	for (auto& actor : m_actors)
	{
		m_window.draw(actor.second);
	}
	if (m_drawFps)
	{
		drawFPS();
	}
}

sf::Uint64 Game::getFrameId()
{
	return m_frameId;
}

void Game::drawFPS()
{
	if (m_frameId % 30 == 0)
	{
		m_tpf = std::max(m_fpsClock.restart().asMicroseconds() / 30, 1LL);
	}
	std::stringstream format;
	std::string text;
	format << "TPF: " << m_tpf << " �s, FPS: " << 1000000 / m_tpf;
	std::getline(format, text);
	m_window.draw(sf::Text(text, m_fpsFont));
}
