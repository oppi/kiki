#pragma once

#include "animation.hpp"

class AnimationState
{
public:
	AnimationState(const Animation* animation, float frameIndex = 0.0f);
	virtual ~AnimationState();

	void update();
	inline void draw(sf::RenderTarget &target, const sf::Transform& transform, sf::RenderStates states) const
	{
		m_animation->draw(target, static_cast<size_t>(m_frameIndex), transform, states);
	}
	void setAnimation(const Animation* animation, float frameIndex = 0.0f);

	float m_speed;

protected:
	const Animation* m_animation;
	float m_frameIndex;
};