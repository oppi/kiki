#pragma once

#include <vector>
#include <list>
#include <unordered_map>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics.hpp>
#include <json/json.h>
#include "sprite.hpp"

class Animation;
typedef std::unordered_map<std::string, Animation> AnimationGroup;

class Animation : sf::NonCopyable
{
public:
	Animation(const std::vector<Sprite>& frames, size_t delay);
	Animation(Animation&& other);
	virtual ~Animation();
	
	size_t getNumFrames() const;
	size_t getFrameDelay() const;
	inline void draw(sf::RenderTarget &target, size_t frame, const sf::Transform& transform, sf::RenderStates states) const
	{
		m_frames.at(frame / std::max(m_delay, 1U)).draw(target, transform, states);
	}

	static std::unordered_map<std::string, AnimationGroup> loadAnimations(const Json::Value& animationGroupsDesc, const std::unordered_map<std::string, Sprite::Sheet>& spriteSheets);

protected:
	const std::vector<Sprite> m_frames;
	const size_t m_delay;
};