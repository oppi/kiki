#pragma once

#include "SFML/Graphics.hpp"
#include "animationState.hpp"

class Actor : public sf::Drawable, public sf::Transformable
{
public:
	Actor(const Animation* animation = nullptr);
	virtual ~Actor();

	void update();
	inline void setAnimation(const Animation* animation)
	{
		m_animationState.setAnimation(animation);
	}

	sf::Vector2f m_velocity;

protected:
	inline void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		m_animationState.draw(target, getTransform(), states);
	}

	AnimationState m_animationState;
};