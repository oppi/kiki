#include "animation.hpp"

Animation::Animation(const std::vector<Sprite>& frames, size_t delay) :
	m_frames(std::move(frames)),
	m_delay(delay)
{
}

Animation::~Animation()
{
}

Animation::Animation(Animation&& other) :
	m_frames(std::move(other.m_frames)),
	m_delay(other.m_delay)
{
}

std::unordered_map<std::string, AnimationGroup> Animation::loadAnimations(const Json::Value& animationGroupsDesc, const std::unordered_map<std::string, Sprite::Sheet>& spriteSheets)
{
	std::unordered_map<std::string, AnimationGroup> animations;
	for (const Json::Value& animationGroupDesc : animationGroupsDesc)
	{
		AnimationGroup& animationGroup = animations.emplace(animationGroupDesc["name"].asString(), AnimationGroup()).first->second;
		for (const Json::Value& animationDesc : animationGroupDesc["animations"])
		{
			std::vector<Sprite> frames;
			frames.reserve(animationDesc["offsets"].size());
			for (const Json::Value& offsetDesc : animationDesc["offsets"])
			{
				frames.emplace_back(spriteSheets.at(animationDesc["sheet"].asString()).sprites.at(offsetDesc.asInt()));
			}
			animationGroup.emplace(animationDesc["name"].asString(), Animation(frames, animationDesc["delay"].asInt()));
		}
	}
	return animations;
}

size_t Animation::getNumFrames() const
{
	return m_frames.size();
}

size_t Animation::getFrameDelay() const
{
	return m_delay;
}