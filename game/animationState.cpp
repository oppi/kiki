#include "animationState.hpp"

AnimationState::AnimationState(const Animation* animation, float frameIndex) :
	m_speed(1.0f),
	m_animation(animation),
	m_frameIndex(frameIndex)
{
}

AnimationState::~AnimationState()
{
}

void AnimationState::setAnimation(const Animation* animation, float frameIndex)
{
	m_animation = animation;
	m_frameIndex = frameIndex;
}

void AnimationState::update()
{
	if (m_animation)
	{
		const size_t delay = m_animation->getFrameDelay();
		if (delay > 0)
		{
			m_frameIndex = std::fmodf(m_frameIndex + m_speed, static_cast<float>(m_animation->getNumFrames() * delay));
		}
	}
}