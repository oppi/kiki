#pragma once

#include <stdint.h>
#include <unordered_map>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics.hpp>
#include <json/json.h>
#include "animation.hpp"
#include "actor.hpp"

class Game : sf::NonCopyable
{
public:
	Game(sf::VideoMode videoMode, const std::string& title);
	virtual ~Game();

	void run();

protected:
	void loadConfig();
	void loadAssets(const Json::Value& assetsDesc);
	virtual void handleEvent(const sf::Event& ev);
	virtual void update();
	virtual void draw();

	sf::Uint64 getFrameId();

	sf::RenderWindow m_window;
	bool m_drawFps;

	std::unordered_map<std::string, Sprite::Sheet> m_spriteSheets;
	std::unordered_map<std::string, AnimationGroup> m_animations;
	std::unordered_map<std::string, Actor> m_actors;

private:
	void drawFPS();

	sf::Uint64 m_frameId;
	sf::Clock m_fpsClock;
	sf::Int64 m_tpf;
	sf::Font m_fpsFont;
};