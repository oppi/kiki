#include "sprite.hpp"

Sprite::Sheet::Sheet()
{
}

Sprite::Sheet::Sheet(Sprite::Sheet&& other) :
	texture(std::move(other.texture)),
	sprites(std::move(other.sprites))
{
}

Sprite::Sprite(const sf::Texture& texture, const sf::FloatRect& rect) :
	m_texture(texture),
	m_rect(rect)
{
}

Sprite::Sprite(const Sprite& other) :
	m_texture(other.m_texture),
	m_rect(other.m_rect)
{
}

Sprite::~Sprite()
{
}

void Sprite::draw(sf::RenderTarget& target, const sf::Transform& transform, sf::RenderStates states) const
{
	float bottom = m_rect.top + m_rect.height;
	float right = m_rect.left + m_rect.width;
	sf::Vertex vertices[4];
	vertices[0].position.x = 0.0f;
	vertices[0].position.y = 0.0f;
	vertices[0].texCoords.x = m_rect.left;
	vertices[0].texCoords.y = m_rect.top;
	vertices[1].position.x = 0.0f;
	vertices[1].position.y = m_rect.height;
	vertices[1].texCoords.x = m_rect.left;
	vertices[1].texCoords.y = bottom;
	vertices[2].position.x = m_rect.width;
	vertices[2].position.y = m_rect.height;
	vertices[2].texCoords.x = right;
	vertices[2].texCoords.y = bottom;
	vertices[3].position.x = m_rect.width;
	vertices[3].position.y = 0.0f;
	vertices[3].texCoords.x = right;
	vertices[3].texCoords.y = m_rect.top;
	states.transform *= transform;
	states.texture = &m_texture;
	target.draw(vertices, 4, sf::PrimitiveType::Quads, states);
}

std::unordered_map<std::string, Sprite::Sheet> Sprite::loadSpriteSheets(const Json::Value& spriteSheetsDesc)
{
	std::unordered_map<std::string, Sprite::Sheet> spriteSheets;
	for (const Json::Value& spriteSheetDesc : spriteSheetsDesc)
	{
		Sprite::Sheet& spriteSheet = spriteSheets.emplace(spriteSheetDesc["name"].asString(), Sprite::Sheet()).first->second;
		spriteSheet.texture.loadFromFile(spriteSheetDesc["file"].asString());
		const sf::Vector2u texSize = spriteSheet.texture.getSize();
		const sf::Vector2u spriteSize(spriteSheetDesc["sprite-width"].asInt(), spriteSheetDesc["sprite-height"].asInt());
		spriteSheet.sprites.reserve(texSize.x * texSize.y / spriteSize.x / spriteSize.y);
		const sf::Vector2f texSizef(static_cast<float>(texSize.x), static_cast<float>(texSize.y));
		const sf::Vector2f spriteSizef(static_cast<float>(spriteSize.x), static_cast<float>(spriteSize.y));
		
		for (float y = 0; y < texSizef.y; y += spriteSizef.y)
		{
			for (float x = 0; x < texSizef.x; x += spriteSizef.x)
			{
				spriteSheet.sprites.emplace_back(spriteSheet.texture, sf::FloatRect(x, y, spriteSizef.x, spriteSizef.y));
			}
		}
	}
	return spriteSheets;
}