#include "player.hpp"
#include <windows.h>

Player::Player(Actor* actor, const AnimationGroup* animations) :
	m_actor(actor),
	m_animations(animations)
{
}

Player::~Player()
{
}

void Player::handleEvent(const sf::Event& ev)
{
	if (!m_actor || !m_animations)
	{
		return;
	}
	switch (ev.type)
	{
		case sf::Event::EventType::KeyPressed:
		case sf::Event::EventType::KeyReleased:
			switch (ev.key.code)
			{
				case sf::Keyboard::W:
				case sf::Keyboard::A:
				case sf::Keyboard::S:
				case sf::Keyboard::D:
					{
						sf::Vector2f vel;
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
						{
							vel.y -= 1.0f;
						}
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
						{
							vel.x -= 1.0f;
						}
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
						{
							vel.y += 1.0f;
						}
						if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
						{
							vel.x += 1.0f;
						}
						const float sqVel = vel.x * vel.x + vel.y * vel.y;
						if (sqVel > 0.0f)
						{
							vel *= 2.0f / std::sqrt(sqVel);
						}
						if (sqVel == 0.0f)
						{
							if (std::fabs(m_actor->m_velocity.x) > std::fabs(m_actor->m_velocity.y))
							{
								if (m_actor->m_velocity.x > 0.0f)
								{
									m_actor->setAnimation(&m_animations->at("stand-right"));
								}
								else
								{
									m_actor->setAnimation(&m_animations->at("stand-left"));
								}
							}
							else
							{
								if (m_actor->m_velocity.y > 0.0f)
								{
									m_actor->setAnimation(&m_animations->at("stand-front"));
								}
								else
								{
									m_actor->setAnimation(&m_animations->at("stand-back"));
								}
							}
						}
						else if (std::fabs(vel.x) > std::fabs(vel.y))
						{
							if (vel.x > 0.0f)
							{
								m_actor->setAnimation(&m_animations->at("walk-right"));
							}
							else
							{
								m_actor->setAnimation(&m_animations->at("walk-left"));
							}
						}
						else
						{
							if (vel.y > 0.0f)
							{
								m_actor->setAnimation(&m_animations->at("walk-front"));
							}
							else
							{
								m_actor->setAnimation(&m_animations->at("walk-back"));
							}
						}
						m_actor->m_velocity = vel;
					}
					break;
			}
	}
}

void Player::setActor(Actor* actor)
{
	m_actor = actor;
	m_actor->setScale(3.0f, 3.0f);
}