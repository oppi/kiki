#include "client.hpp"

Client::Client(sf::VideoMode videoMode, const std::string& title) :
	Game(videoMode, title),
	m_player(nullptr, &m_animations.at("kairi"))
{
	m_player.setActor(&m_actors.emplace("kairi", &m_animations.at("kairi").at("stand-front")).first->second);
	m_actors.emplace("background", &m_animations.at("background").at("default"));
}

Client::~Client()
{
}

void Client::handleEvent(const sf::Event& ev)
{
	Game::handleEvent(ev);
	m_player.handleEvent(ev);
}

void Client::update()
{
	Game::update();
}