#include <game.hpp>
#include "player.hpp"

class Client : public Game
{
public:
	Client(sf::VideoMode videoMode, const std::string& title);
	virtual ~Client();

	void handleEvent(const sf::Event& ev) override;
	void update() override;

protected:
	Player m_player;

};