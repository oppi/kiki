#include <actor.hpp>

class Player
{
public:
	Player(Actor* actor = nullptr, const AnimationGroup* animations = nullptr);
	virtual ~Player();

	void handleEvent(const sf::Event& ev);
	void setActor(Actor* actor);

protected:
	Actor* m_actor;
	const AnimationGroup* m_animations;
};